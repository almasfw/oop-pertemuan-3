open class Hewan(open var alatNafas: String = "") : MakhlukHidup() {
    override fun bernafas() {
        println("Bernafas dengan ${alatNafas}")
    }
}