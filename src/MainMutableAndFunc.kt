fun main(args: Array<String>) {
    val a = mutableListOf(5, 2, 4, 8, 1, 10, 14, 16, 23, 0)

    //println("Rata-rata: ${rata(a)}")
    //println("Rata-rata: ${a.average()}")
    //println("Nilai maksimal: ${nilaiMax(a)}")
    //println("Nilai maksimal: ${a.max()}")
    //println("Nilai minimal: ${nilaiMin(a)}")
    //println("Nilai minimal: ${a.min()}")

    val cal = Calculator(a)
    println("Data: ${cal.data}")
    println("Rata-rata: ${cal.rata()}")
    println("Nilai maksimal: ${cal.nilaiMax()}")
    println("Nilai minimal: ${cal.nilaiMin()}")
}

/*
fun rata(list: List<Int>): Int {
    var jml = 0
    list.forEach() {
        jml += it
    }

    return jml / list.size
}

fun nilaiMax(list: List<Int>): Int {
    var n = 0
    list.forEach() {
        if (it > n) {
            n = it
        }
    }

    return n
}

fun nilaiMin(list: List<Int>): Int {
    var n = 999
    list.forEach() {
        if (it < n) {
            n = it
        }
    }

    return n
}
*/