class Ikan(var habitat: String, override var alatNafas: String) : Hewan(alatNafas) {
    fun berenang() {
        println("Di habitat $habitat dengan $alatNafas")
    }

    override fun bernafas(){
        println("Di habitat $habitat dengan $alatNafas")
    }
}