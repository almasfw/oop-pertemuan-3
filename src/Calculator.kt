data class Calculator(val data: List<Int>) {     //data class, jadi udah punya setter dan getter
    fun nilaiMax(): Int {
        var n = 0
        data.forEach() {
            if (it > n) {
                n = it
            }
        }

        return n
    }

    fun nilaiMin(): Int {
        var n = 999
        data.forEach() {
            if (it < n) {
                n = it
            }
        }

        return n
    }

    fun rata(): Int {
        var jml = 0
        data.forEach() {
            jml += it
        }

        return jml / data.size
    }
}